/* START: función main */


function main() {
    var start_time = Date.now();

    var validacion = true;
    if ($("#flujo_solvente").val() == "") validacion = false;
    if ($("#temperatura_solvente").val() == "") validacion = false;
    if ($("#presion_solvente").val() == "") validacion = false;
    if ($("#flujo_gas").val() == "") validacion = false;
    if ($("#temperatura_gas").val() == "") validacion = false;
    if ($("#presion_gas").val() == "") validacion = false;

    /* START: Declaración de variables dadas por el usuario */

    /* Componentes */

    var composicion_entrada = [];

    if ($("#monoetanolamina_radio").is(':checked') == true) {

        if ($("#pentano_checkbox").is(':checked') == true) {
            if ($("#pentano_input").val() == "") validacion = false;
            var pentano = parseFloat($("#pentano_input").val()).toFixed(4);
            composicion_entrada.push([
                'pentano', pentano
            ]);
        }
        if ($("#sulfuro_de_hidrogeno_checkbox").is(':checked') == true) {
            if ($("#sulfuro_de_hidrogeno_input").val() == "") validacion = false;
            var sulfuro_de_hidrogeno = parseFloat($("#sulfuro_de_hidrogeno_input").val()).toFixed(4);
            composicion_entrada.push([
                'sulfuro_de_hidrogeno', sulfuro_de_hidrogeno
            ]);
        }
        if ($("#butano_checkbox").is(':checked') == true) {
            if ($("#butano_input").val() == "") validacion = false;
            var butano = parseFloat($("#butano_input").val()).toFixed(4);
            composicion_entrada.push([
                'butano', butano
            ]);
        }
        if ($("#propano_checkbox").is(':checked') == true) {
            if ($("#propano_input").val() == "") validacion = false;
            var propano = parseFloat($("#propano_input").val()).toFixed(4);
            composicion_entrada.push([
                'propano', propano
            ]);
        }
        if ($("#dioxido_de_carbono_checkbox").is(':checked') == true) {
            if ($("#dioxido_de_carbono_input").val() == "") validacion = false;
            var dioxido_de_carbono = parseFloat($("#dioxido_de_carbono_input").val()).toFixed(4);
            composicion_entrada.push([
                'dioxido_de_carbono', dioxido_de_carbono
            ]);
        }
        if ($("#etano_checkbox").is(':checked') == true) {
            if ($("#etano_input").val() == "") validacion = false;
            var etano = parseFloat($("#etano_input").val()).toFixed(4);
            composicion_entrada.push([
                'etano', etano
            ]);
        }
        if ($("#metano_checkbox").is(':checked') == true) {
            if ($("#metano_input").val() == "") validacion = false;
            var metano = parseFloat($("#metano_input").val()).toFixed(4);
            composicion_entrada.push([
                'metano', metano]);
        }
        if ($("#aire_checkbox").is(':checked') == true) {
            if ($("#aire_input").val() == "") validacion = false;
            var aire = parseFloat($("#aire_input").val()).toFixed(4);
            composicion_entrada.push([
                'aire', aire]);
        }
        composicion_entrada.push([
            'monoetanolamina', 1.0
        ]);
    }
    if ($("#diglicolamina_radio").is(':checked') == true) {

        if ($("#pentano_checkbox").is(':checked') == true) {
            if ($("#pentano_input").val() == "") validacion = false;
            var pentano = parseFloat($("#pentano_input").val()).toFixed(4);
            composicion_entrada.push([
                'pentano', pentano
            ]);
        }
        if ($("#butano_checkbox").is(':checked') == true) {
            if ($("#butano_input").val() == "") validacion = false;
            var butano = parseFloat($("#butano_input").val()).toFixed(4);
            composicion_entrada.push([
                'butano', butano
            ]);
        }
        if ($("#sulfuro_de_hidrogeno_checkbox").is(':checked') == true) {
            if ($("#sulfuro_de_hidrogeno_input").val() == "") validacion = false;
            var sulfuro_de_hidrogeno = parseFloat($("#sulfuro_de_hidrogeno_input").val()).toFixed(4);
            composicion_entrada.push([
                'sulfuro_de_hidrogeno', sulfuro_de_hidrogeno
            ]);
        }

        if ($("#propano_checkbox").is(':checked') == true) {
            if ($("#propano_input").val() == "") validacion = false;
            var propano = parseFloat($("#propano_input").val()).toFixed(4);
            composicion_entrada.push([
                'propano', propano
            ]);
        }
        if ($("#dioxido_de_carbono_checkbox").is(':checked') == true) {
            if ($("#dioxido_de_carbono_input").val() == "") validacion = false;
            var dioxido_de_carbono = parseFloat($("#dioxido_de_carbono_input").val()).toFixed(4);
            composicion_entrada.push([
                'dioxido_de_carbono', dioxido_de_carbono
            ]);
        }
        if ($("#etano_checkbox").is(':checked') == true) {
            if ($("#etano_input").val() == "") validacion = false;
            var etano = parseFloat($("#etano_input").val()).toFixed(4);
            composicion_entrada.push([
                'etano', etano
            ]);
        }
        if ($("#metano_checkbox").is(':checked') == true) {
            if ($("#metano_input").val() == "") validacion = false;
            var metano = parseFloat($("#metano_input").val()).toFixed(4);
            composicion_entrada.push([
                'metano', metano]);
        }
        if ($("#aire_checkbox").is(':checked') == true) {
            if ($("#aire_input").val() == "") validacion = false;
            var aire = parseFloat($("#aire_input").val()).toFixed(4);
            composicion_entrada.push([
                'aire', aire]);
        }
        composicion_entrada.push([
            'diglicolamina', 1.0
        ]);


    }
    if ($("#metildietanolamina_radio").is(':checked') == true) {
        if ($("#pentano_checkbox").is(':checked') == true) {
            if ($("#pentano_input").val() == "") validacion = false;
            var pentano = parseFloat($("#pentano_input").val()).toFixed(4);
            composicion_entrada.push([
                'pentano', pentano
            ]);
        }
        if ($("#butano_checkbox").is(':checked') == true) {
            if ($("#butano_input").val() == "") validacion = false;
            var butano = parseFloat($("#butano_input").val()).toFixed(4);
            composicion_entrada.push([
                'butano', butano
            ]);
        }
        if ($("#sulfuro_de_hidrogeno_checkbox").is(':checked') == true) {
            if ($("#sulfuro_de_hidrogeno_input").val() == "") validacion = false;
            var sulfuro_de_hidrogeno = parseFloat($("#sulfuro_de_hidrogeno_input").val()).toFixed(4);
            composicion_entrada.push([
                'sulfuro_de_hidrogeno', sulfuro_de_hidrogeno
            ]);
        }

        if ($("#propano_checkbox").is(':checked') == true) {
            if ($("#propano_input").val() == "") validacion = false;
            var propano = parseFloat($("#propano_input").val()).toFixed(4);
            composicion_entrada.push([
                'propano', propano
            ]);
        }
        if ($("#dioxido_de_carbono_checkbox").is(':checked') == true) {
            if ($("#dioxido_de_carbono_input").val() == "") validacion = false;
            var dioxido_de_carbono = parseFloat($("#dioxido_de_carbono_input").val()).toFixed(4);
            composicion_entrada.push([
                'dioxido_de_carbono', dioxido_de_carbono
            ]);
        }
        if ($("#etano_checkbox").is(':checked') == true) {
            if ($("#etano_input").val() == "") validacion = false;
            var etano = parseFloat($("#etano_input").val()).toFixed(4);
            composicion_entrada.push([
                'etano', etano
            ]);
        }
        if ($("#metano_checkbox").is(':checked') == true) {
            if ($("#metano_input").val() == "") validacion = false;
            var metano = parseFloat($("#metano_input").val()).toFixed(4);
            composicion_entrada.push([
                'metano', metano]);
        }
        if ($("#aire_checkbox").is(':checked') == true) {
            if ($("#aire_input").val() == "") validacion = false;
            var aire = parseFloat($("#aire_input").val()).toFixed(4);
            composicion_entrada.push([
                'aire', aire]);
        }

        composicion_entrada.push([
            'metildietanolamina', 1.0
        ]);
    }
    if ($("#dimetilamina_radio").is(':checked') == true) {
        if ($("#pentano_checkbox").is(':checked') == true) {
            if ($("#pentano_input").val() == "") validacion = false;
            var pentano = parseFloat($("#pentano_input").val()).toFixed(4);
            composicion_entrada.push([
                'pentano', pentano
            ]);
        }
        if ($("#butano_checkbox").is(':checked') == true) {
            if ($("#butano_input").val() == "") validacion = false;
            var butano = parseFloat($("#butano_input").val()).toFixed(4);
            composicion_entrada.push([
                'butano', butano
            ]);
        }
        if ($("#propano_checkbox").is(':checked') == true) {
            if ($("#propano_input").val() == "") validacion = false;
            var propano = parseFloat($("#propano_input").val()).toFixed(4);
            composicion_entrada.push([
                'propano', propano
            ]);
        }
        if ($("#sulfuro_de_hidrogeno_checkbox").is(':checked') == true) {
            if ($("#sulfuro_de_hidrogeno_input").val() == "") validacion = false;
            var sulfuro_de_hidrogeno = parseFloat($("#sulfuro_de_hidrogeno_input").val()).toFixed(4);
            composicion_entrada.push([
                'sulfuro_de_hidrogeno', sulfuro_de_hidrogeno
            ]);
        }
        if ($("#etano_checkbox").is(':checked') == true) {
            if ($("#etano_input").val() == "") validacion = false;
            var etano = parseFloat($("#etano_input").val()).toFixed(4);
            composicion_entrada.push([
                'etano', etano
            ]);
        }
        if ($("#dioxido_de_carbono_checkbox").is(':checked') == true) {
            if ($("#dioxido_de_carbono_input").val() == "") validacion = false;
            var dioxido_de_carbono = parseFloat($("#dioxido_de_carbono_input").val()).toFixed(4);
            composicion_entrada.push([
                'dioxido_de_carbono', dioxido_de_carbono
            ]);
        }
        if ($("#metano_checkbox").is(':checked') == true) {
            if ($("#metano_input").val() == "") validacion = false;
            var metano = parseFloat($("#metano_input").val()).toFixed(4);
            composicion_entrada.push([
                'metano', metano]);
        }
        if ($("#aire_checkbox").is(':checked') == true) {
            if ($("#aire_input").val() == "") validacion = false;
            var aire = parseFloat($("#aire_input").val()).toFixed(4);
            composicion_entrada.push([
                'aire', aire]);
        }
        composicion_entrada.push([
            'dimetilamina', 1.0
        ]);
    }
    var sumaComponentes = 0;
    for (i = 0; i < composicion_entrada.length; i++) {
        sumaComponentes += parseFloat(composicion_entrada[i][1]);
    }
    console.log(sumaComponentes);
    if (sumaComponentes != 2) validacion = false;
    if (validacion == true) {
        console.log("Componenetes de entrada: ")
        console.log(composicion_entrada);

        /* Liquidos (Solventes)*/
        var flujo_solvente = parseFloat($("#flujo_solvente").val());
        var temperatura_solvente = parseFloat($("#temperatura_solvente").val());
        var presion_solvente = parseFloat($("#presion_solvente").val());


        /* Gases */
        var flujo_gas = parseFloat($("#flujo_gas").val());
        var temperatura_gas = parseFloat($("#temperatura_gas").val());
        var presion_gas = parseFloat($("#presion_gas").val());

        /* Torre */
        var presion_torre_inicial = parseFloat($("#presion_torre_inicial").val());
        var presion_torre_final = parseFloat($("#presion_torre_final").val());
        /* END: Declaración de variables dadas por el usuario */

        /* START: Declaración de constantes */
        const numero_etapas = parseInt($("#numero_etapas").val());
        // Tc, Pc, Wi, aj0, aj1, aj2, aj3, aj4
        const componentes = [
            ['metano', 343.08, 667.2124352, 0.012, 0.8245223, 0.03806383, 0.000008864745, -0.00000000461153, 0.000000000001822959],
            ['etano', 549.54, 706.8186528, 0.1, 11.51606, 0.0140309, 0.00000085403, -0.0000000106078, 0.000000000003162199],
            ['propano', 665.64, 616.2901554, 0.152, 15.68683, 0.02504953, 0.00001404258, -0.0000000352626, 0.00000000001864467],
            ['butano', 765.18, 550.7150259, 0.2, 20.79783, 0.03143287, 0.00001929511, -0.00000004538652, 0.00000000002380972],
            ['pentano', 845.46, 488.9119171, 0.252, 25.64627, 0.0389176, 0.00002397294, -0.00000005842615, 0.0000000003079918],
            ['sulfuro_de_hidrogeno', 672.3, 1300.331606, 0.094, 8.031194, 0.0009868632, 0.000002388543, 0.00000000159311, 0.000000000000320326],
            ['dioxido_de_carbono', 547.56, 1071.108808, 0.224, 8.348605, -0.006475766, -0.000003555025, 0.000000001194595, -0.0000000000001851702],
            ['aire', 658.026, 671.5247, 0.137, 13.63267, 0.02106998, 0.00000249845, -0.00000001146863, 0.000000000005247386],
            ['dimetilamina', 996.44, 548.2426, 0.212, 21.00016, 0.05627391, 0.00001129438, -0.00000003606168, 0.00000000001482606],
            ['monoetanolamina', 959.022, 548.2426, 0.230, 22.02735, 0.05465972, 0.000005935, 0.00000003442294, 0.0000000000164543],
            ['diglicolamina', 1144.8, 554.0442, 0.295, 24.82866, 0.05843, -0.000025693, 0.000000003432486, 0.0000000000008297016],
            ['metildietanolamina', 1389.6, 578.7006, 0.296, 29.351, 0.0747558, -0.00001560299, -0.00000001428024, 0.000000000007089441],
        ];

        /* END: Declaración de constantes */
        var qj = [];
        for (j = 0; j < numero_etapas; j++) {
            qj.push(parseFloat($("#etapa_" + (j + 1)).val()));
        }
        console.log(qj);
        /* START: Busca datos en en la base de datos y los añade */
        const Tc = [];
        const Pc = [];
        const Wi = [];
        const aj0 = [];
        const aj1 = [];
        const aj2 = [];
        const aj3 = [];
        const aj4 = [];
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < componentes.length; j++) {
                if (composicion_entrada[i][0] == componentes[j][0]) {
                    Tc.push(componentes[j][1]);
                    Pc.push(componentes[j][2]);
                    Wi.push(componentes[j][3]);
                    aj0.push(componentes[j][4]);
                    aj1.push(componentes[j][5]);
                    aj2.push(componentes[j][6]);
                    aj3.push(componentes[j][7]);
                    aj4.push(componentes[j][8]);
                }
            }
        }
        console.log("Tc");
        console.log(Tc);
        console.log("Pc");
        console.log(Pc);
        console.log("Wi");
        console.log(Wi);
        console.log("aj0");
        console.log(aj0);
        console.log("aj1");
        console.log(aj1);
        console.log("aj2");
        console.log(aj2);
        console.log("aj3");
        console.log(aj3);
        console.log("aj4");
        console.log(aj4);

        /* START: Calculo de tjInit */
        var TjInit = [];
        for (j = 0; j < numero_etapas; j++) {
            if (j == 0) {
                TjInit[j] = temperatura_solvente;
            } else if (j == (numero_etapas - 1)) {
                TjInit[j] = temperatura_gas;
            } else {
                TjInit[j] = TjInit[j - 1] + ((temperatura_gas - temperatura_solvente) / (numero_etapas - 1));
            }
        }
        console.log("TjInit:");
        console.log(TjInit);
        /* END: Calculo de tjInit */

        /* START: Calculo de PjInit */
        var PjInit = [];
        for (j = 0; j < numero_etapas; j++) {
            if (j == 0) {
                PjInit[j] = presion_torre_inicial;
            }
            else {
                PjInit[j] = PjInit[j - 1] + presion_torre_final;
            }
        }
        console.log("PjInit:");
        console.log(PjInit);
        console.log("PjInit:");
        console.log(PjInit);
        /* END: Calculo de PjInit */

        var mi = [];
        var aij = createMatrix();

        for (i = 0; i < composicion_entrada.length; i++) {
            mi[i] = 0.480 + 1.574 * Wi[i] - 0.176 * Math.pow(Wi[i], 2);
        }

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aij[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((TjInit[j] / Tc[i]), 0.5)), 2);
            }
        }
        console.log("mi:");
        console.log(mi);
        console.log("aij:");
        console.log(aij);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aij y Bij */
        var Aij = createMatrix();
        var Bij = createMatrix();

        /* START Realiza ecuación Aij y Bij*/
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                Aij[i][j] = (0.42747 * aij[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(TjInit[j] / Tc[i], 2)));
                Bij[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (TjInit[j] / Tc[i]));
            }
        }
        console.log("Aij:");
        console.log(Aij);
        console.log("Bij:");
        console.log(Bij);
        /* END Realiza ecuación Aij y Bij */

        /* START: Obtiene de mayor prioridad */
        var yInit = createMatrix();
        /* STRAT : Calculo composicion  y1 */

        yInit[0][0] = 0;
        yInit[0][(numero_etapas - 1)] = composicion_entrada[0][1] - (composicion_entrada[0][1] / (numero_etapas));
        for (j = (numero_etapas - 2); j > 0; j--) {
            yInit[0][j] = yInit[0][j + 1] - (composicion_entrada[0][1] / numero_etapas);
        }

        var VjInit = [];
        for (j = 0; j < numero_etapas; j++) {
            VjInit[j] = ((1 - composicion_entrada[0][1]) * flujo_gas) / (1 - yInit[0][j]);
        }
        console.log("VjInit");
        console.log(VjInit);

        for (i = 1; i < composicion_entrada.length; i++) {
            for (j = (numero_etapas - 1); j >= 0; j--) {
                if (j == (numero_etapas - 1)) {
                    yInit[i][j] = (composicion_entrada[i][1] * flujo_gas) / VjInit[j];
                } else {
                    yInit[i][j] = (yInit[i][j + 1] * VjInit[j + 1]) / VjInit[j];
                }
                if (i == (composicion_entrada.length - 1)) {
                    yInit[i][j] = 0;
                }
            }
        }
        console.log("yInit");
        console.log(yInit);

        /* cálculo de Reglas de Mezcla */

        var matrizMultiplicacionAy = createMatrix();
        var matrizMultiplicacionBy = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                matrizMultiplicacionAy[i][j] = Aij[i][j] * yInit[i][j];
                matrizMultiplicacionBy[i][j] = Bij[i][j] * yInit[i][j];
            }
        }
        var Aj = [];
        var Bj = [];
        for (j = 0; j < numero_etapas; j++) {
            var auxAy = 0;
            var auxBy = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                auxAy += matrizMultiplicacionAy[i][j];
                auxBy += matrizMultiplicacionBy[i][j];
            }
            Aj[j] = auxAy;
            Bj[j] = auxBy;
        }
        console.log("Aj");
        console.log(Aj);
        console.log("Bj");
        console.log(Bj);

        /* START Entapía Alimentación  */

        /* START Ecuación de tercer grado */
        var a = [];
        var b = [];
        var c = [];
        var mp = [];
        var mq = [];
        var Delta = [];
        var z1 = [];
        var z2 = [];
        var z3 = [];
        for (j = 0; j < numero_etapas; j++) {
            a[j] = -1;
            b[j] = Aj[j] - Bj[j] - Math.pow(Bj[j], 2);
            c[j] = -(Aj[j] * Bj[j]);
        }
        for (j = 0; j < numero_etapas; j++) {

            mp[j] = (3 * b[j] - Math.pow(a[j], 2)) / 3;


            mq[j] = (2 * Math.pow(a[j], 3) - 9 * a[j] * b[j] + 27 * c[j]) / 27;

            Delta[j] = Math.pow(mq[j] / 2, 2) + Math.pow(mp[j] / 3, 3);
        }
        for (j = 0; j < numero_etapas; j++) {
            z1[j] = Math.pow(((-mq[j] / 2) + Math.pow(Delta[j], 1 / 2)), 1 / 3) + (-1 * Math.pow(((mq[j] / 2) + Math.pow(Delta[j], 1 / 2)), 1 / 3)) - (a[j] / 3);
        }
        for (j = 0; j < numero_etapas; j++) {
            z2[j] = - (z1[j] / 2) + Math.sqrt(Math.pow(z1[j] / 2, 2) + mq[j] / z1[j]) - a[j] / 3;
            z3[j] = - (z1[j] / 2) - Math.sqrt(Math.pow(z1[j] / 2, 2) + mq[j] / z1[j]) - a[j] / 3;
        }
        console.log("z1");
        console.log(z1);
        console.log("z2");
        console.log(z2);
        console.log("z3");
        console.log(z3);

        var phiLj = createMatrix();
        var phiVj = createMatrix();
        var kij = createMatrix();

        /* START cálculo de phi */
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                phiVj[i][j] = Math.exp((z2[j] - 1) * (Bij[i][j] / Bj[j]) - Math.log(z2[j] - Bj[j]) - (Aj[j] / Bj[j]) * ((2 * Math.pow(Aij[i][j], 0.5) / Math.pow(Aj[j], 0.5)) - (Bij[i][j]) / Bj[j])) * (Math.log((z2[j] + Bj[j]) / z2[j]));
                phiLj[i][j] = Math.exp((z1[j] - 1) * (Bij[i][j] / Bj[j]) - Math.log(z1[j] - Bj[j]) - (Aj[j] / Bj[j]) * ((2 * Math.pow(Aij[i][j], 0.5) / Math.pow(Aj[j], 0.5)) - (Bij[i][j]) / Bj[j])) * (Math.log((z1[j] + Bj[j]) / z1[j]));
            }
        }

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                if (phiVj[i][j] == 0)
                    phiVj[i][j] = 1;
            }
        }

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                if (phiLj[i][j] == 0)
                    phiLj[i][j] = 1;
            }
        }

        /* START cálculo Constante de equilibrio */
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                kij[i][j] = phiLj[i][j] / phiVj[i][j];
            }
        }
        console.log("phiLj");
        console.log(phiLj);
        console.log("phiVj");
        console.log(phiVj);
        console.log("kij");
        console.log(kij);

        /* END Constante de equilibrio */
        /* Inicio Algoritmo de Thomas para composiciones liquidas */
        var A = createMatrix();
        var B = createMatrix();
        var C = createMatrix();
        var D = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                if (j == (numero_etapas - 1)) { //Ultima etapa

                    A[i][j] = VjInit[j] + flujo_gas - VjInit[0];
                    B[i][j] = flujo_gas - VjInit[0] + (VjInit[j] * kij[i][j]);
                    C[i][j] = 0;
                    D[i][j] = -(flujo_gas * composicion_entrada[i][1]);
                }
                else { // Primera etapa e intermedias
                    A[i][j] = VjInit[j] + flujo_solvente - VjInit[0];
                    D[i][j] = -(flujo_solvente * composicion_entrada[i][1]);
                    C[i][j] = VjInit[j + 1] * kij[i][j + 1];
                    B[i][j] = -(VjInit[j + 1] + flujo_solvente - VjInit[0] + (VjInit[j] * kij[i][j]));
                }
            }
        }
        console.log("A");
        console.log(A);
        console.log("B");
        console.log(B);
        console.log("C");
        console.log(C);
        console.log("D");
        console.log(D);

        var qji = createMatrix();
        var pji = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                if (j == 0) { // Primera etapa
                    pji[i][j] = C[i][j] / B[i][j];
                    qji[i][j] = D[i][j] / B[i][j];
                }
                else if (j == (numero_etapas - 1)) { //Ultima Etapa
                    pji[i][j] = 0;
                    qji[i][j] = (D[i][j] - A[i][j] * pji[i][j - 1]) / (B[i][j] - A[i][j] * pji[i][j - 1]);
                }
                else { //Intermedio
                    pji[i][j] = C[i][j] / (B[i][j] - A[i][j] * pji[i][j - 1]);
                    qji[i][j] = (D[i][j] - A[i][j] * pji[i][j - 1]) / (B[i][j] - A[i][j] * pji[i][j - 1]);
                }
            }
        }
        console.log("qji");
        console.log(qji);
        console.log("pji");
        console.log(pji);

        var xji = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = (numero_etapas - 1); j > -1; j--) {
                if (j == (numero_etapas - 1)) { //Ultima Etapa
                    xji[i][j] = qji[i][j];
                }
                else { //Intermedias y primera
                    xji[i][j] = qji[i][j] - pji[i][j] * xji[i][j + 1];
                }
            }
        }
        console.log("xji");
        console.log(xji);
        /* END Algoritmo de Thomas para composiciones liquidas */

        /* START Calcula Lj0 */

        var Lj0 = [];

        for (j = 0; j < numero_etapas; j++) {
            if (j == (numero_etapas - 1)) {
                Lj0[j] = (flujo_solvente + flujo_gas) - VjInit[0];
            } else {
                Lj0[j] = VjInit[j + 1] + flujo_solvente - VjInit[0];
            }
        }
        console.log("Lj0");
        console.log(Lj0);
        /* END Calcula Lj0 */

        /* START calculo suma x */
        var sumaX = [];
        for (j = 0; j < numero_etapas; j++) {
            sumaXinit = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                sumaXinit += xji[i][j];
            }
            sumaX.push(sumaXinit);
        }

        var Lj = [];
        for (j = 0; j < numero_etapas; j++) {
            Lj[j] = Lj0[j] * Math.abs(sumaX[j]);
        }
        console.log("Lj");
        console.log(Lj);
        /* END calculo Lj */

        /* START Calcula Vj */
        var Vj = [];
        for (j = 0; j < numero_etapas; j++) {
            if (j == 0) {
                Vj[j] = -Lj[numero_etapas - 1] + flujo_solvente + flujo_gas;
            }
            else {
                Vj[j] = Lj[j - 1] - Lj[numero_etapas - 1] + flujo_gas;
            }
        }
        console.log("Vj");
        console.log(Vj);
        /* END Calcula Vj */

        /*START Normalización de x & y*/
        var xNormalizado = createMatrix();
        var yNormalizado = createMatrix();
        var sumaYnorm = [];
        var sumaXnorm = [];
        var yNorm = createMatrix();
        for (j = 0; j < numero_etapas; j++) {
            sumaXAux = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                sumaXAux += xji[i][j];
            }
            sumaXnorm.push(sumaXAux);
        }

        for (j = 0; j < numero_etapas; j++) {
            for (i = 0; i < composicion_entrada.length; i++) {
                xNormalizado[i][j] = (xji[i][j] / sumaXnorm[j]);
            }
        }
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                yNorm[i][j] = kij[i][j] * xNormalizado[i][j];
            }
        }

        console.log("yNorm");
        console.log(yNorm);

        //Normalización de Y
        for (j = 0; j < numero_etapas; j++) {
            sumaYAux = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                sumaYAux += yNorm[i][j];
            }
            sumaYnorm.push(sumaYAux);
        }
        for (j = 0; j < numero_etapas; j++) {
            for (i = 0; i < composicion_entrada.length; i++) {

                yNormalizado[i][j] = (yNorm[i][j] / sumaYnorm[j]);
            }
        }
        console.log("xNormalizado");
        console.log(xNormalizado);
        console.log("yNormalizado");
        console.log(yNormalizado);

        /* END Normalización de x & y */
        tmash = [];
        tmenosh = [];
        h = [];
        for (j = 0; j < numero_etapas; j++) {
            tmash[j] = TjInit[j] + (TjInit[j] * 1e-6);
            tmenosh[j] = TjInit[j] - (TjInit[j] * 1e-6);
            h[j] = 2 * TjInit[j] * 1e-6;
        }
        console.log("tmash");
        console.log(tmash);
        console.log("tmenosh");
        console.log(tmenosh);
        console.log("h");
        console.log(h);

        /*ENTALPIA FUNCION tmenosh[j]*/
        const T0 = 0;
        const Rconst = 1.987;
        aj1ytmenosh = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aj1ytmenosh[i][j] = ((aj0[i] * (tmenosh[j] - T0)) + (aj1[i] * ((Math.pow(tmenosh[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmenosh[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmenosh[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmenosh[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1ytmenosh");
        console.log(aj1ytmenosh);

        sumaj1ktmenosh = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumaj1ytmenosh = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumaj1ytmenosh += aj1ytmenosh[i][j];
            }
            sumaj1ktmenosh[j] = iniciosumaj1ytmenosh;
        }
        console.log("sumaj1ktmenosh");
        console.log(sumaj1ktmenosh);

        var aijtmenosh = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aijtmenosh[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmenosh[j] / Tc[i]), 0.5)), 2);
            }
        }
        console.log("aijtmenosh");
        console.log(aijtmenosh);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmenosh y Bijtmenosh  en función de tmenosH*/
        var Aijtmenosh = createMatrix();
        var Bijtmenosh = createMatrix();

        /* START Realiza ecuación Aijtmenosh y Bijtmenosh*/

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                Aijtmenosh[i][j] = (0.42747 * aijtmenosh[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(tmenosh[j] / Tc[i], 2)));
                Bijtmenosh[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (tmenosh[j] / Tc[i]));
            }
        }
        console.log("Aijtmenosh");
        console.log(Aijtmenosh);
        console.log("Bijtmenosh");
        console.log(Bijtmenosh);
        /* END Realiza ecuación Ajtmenosh y Bjtmenosh */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAjtmenosh = createMatrix();
        matrizMultiplicacionBjtmenosh = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                matrizMultiplicacionAjtmenosh[i][j] = Aijtmenosh[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBjtmenosh[i][j] = Bijtmenosh[i][j] * yNormalizado[i][j];
            }
        }

        Ajtmenosh = [];
        Bjtmenosh = [];
        for (j = 0; j < numero_etapas; j++) {
            auxAjtmenosh = 0;
            auxBjtmenosh = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                auxAjtmenosh += matrizMultiplicacionAjtmenosh[i][j];
                auxBjtmenosh += matrizMultiplicacionBjtmenosh[i][j];
            }
            Ajtmenosh.push(auxAjtmenosh);
            Bjtmenosh.push(auxBjtmenosh);
        }

        doblesumAiktmenosh = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (k = 0; k < composicion_entrada.length; k++) {
                for (j = 0; j < numero_etapas; j++) {
                    doblesumAiktmenosh[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aijtmenosh[i][j] * Aijtmenosh[k][j]) * (1 - mi[i] * Math.pow(tmenosh[j] / Tc[i], 0.5) / (2 * aijtmenosh[i][j]) - mi[k] * Math.pow(tmenosh[j] / Tc[k], 0.5) / (2 * aijtmenosh[k][j]));
                }
            }
        }
        console.log("doblesumAiktmenosh");
        console.log(doblesumAiktmenosh);

        doblesumTotaltmenosh = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumAiktmenosh = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumAiktmenosh += doblesumAiktmenosh[i][j];
            }
            doblesumTotaltmenosh[j] = iniciosumAiktmenosh;
        }

        HvjTmenosh = [];
        HljTmenosh = [];
        for (j = 0; j < numero_etapas; j++) {
            HvjTmenosh[j] = Rconst * sumaj1ktmenosh[j] + Rconst * tmenosh[j] * (z2[j] - 1 - (1 / Bjtmenosh[j]) * Math.log((z2[j] + Bjtmenosh[j]) / z2[j]) * (doblesumTotaltmenosh[j]));
            HljTmenosh[j] = Rconst * sumaj1ktmenosh[j] + Rconst * tmenosh[j] * (z1[j] - 1 - (1 / Bjtmenosh[j]) * Math.log((z1[j] + Bjtmenosh[j]) / z1[j]) * (doblesumTotaltmenosh[j]));
        }
        console.log("HvjTmenosh");
        console.log(HvjTmenosh);
        console.log("HljTmenosh");
        console.log(HljTmenosh);

        /*ENTALPIA FUNCION tmash[j]*/
        aj1ytmash = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aj1ytmash[i][j] = ((aj0[i] * (tmash[j] - T0)) + (aj1[i] * ((Math.pow(tmash[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmash[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmash[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmash[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1ytmash");
        console.log(aj1ytmash);

        sumaj1ktmash = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumaj1ytmash = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumaj1ytmash += aj1ytmash[i][j];
            }
            sumaj1ktmash[j] = iniciosumaj1ytmash;
        }
        console.log("sumaj1ktmash");
        console.log(sumaj1ktmash);

        var aijtmash = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aijtmash[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmash[j] / Tc[i]), 0.5)), 2);
            }
        }
        console.log("aijtmash");
        console.log(aijtmash);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmash y Bijtmash  en función de tmash*/
        var Aijtmash = createMatrix();
        var Bijtmash = createMatrix();

        /* START Realiza ecuación Aijtmash y Bijtmash*/

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                Aijtmash[i][j] = (0.42747 * aijtmash[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(tmash[j] / Tc[i], 2)));
                Bijtmash[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (tmash[j] / Tc[i]));
            }
        }
        console.log("Aijtmash");
        console.log(Aijtmash);
        console.log("Bijtmash");
        console.log(Bijtmash);
        /* END Realiza ecuación Ajtmash y Bjtmash */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAjtmash = createMatrix();
        matrizMultiplicacionBjtmash = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                matrizMultiplicacionAjtmash[i][j] = Aijtmash[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBjtmash[i][j] = Bijtmash[i][j] * yNormalizado[i][j];
            }
        }

        Ajtmash = [];
        Bjtmash = [];
        for (j = 0; j < numero_etapas; j++) {
            auxAjtmash = 0;
            auxBjtmash = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                auxAjtmash += matrizMultiplicacionAjtmash[i][j];
                auxBjtmash += matrizMultiplicacionBjtmash[i][j];
            }
            Ajtmash.push(auxAjtmash);
            Bjtmash.push(auxBjtmash);
        }

        doblesumAiktmash = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (k = 0; k < composicion_entrada.length; k++) {
                for (j = 0; j < numero_etapas; j++) {
                    doblesumAiktmash[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aijtmash[i][j] * Aijtmash[k][j]) * (1 - mi[i] * Math.pow(tmash[j] / Tc[i], 0.5) / (2 * aijtmash[i][j]) - mi[k] * Math.pow(tmash[j] / Tc[k], 0.5) / (2 * aijtmash[k][j]));
                }
            }
        }
        console.log("doblesumAiktmash");
        console.log(doblesumAiktmash);

        doblesumTotaltmash = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumAiktmash = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumAiktmash += doblesumAiktmash[i][j];
            }
            doblesumTotaltmash[j] = iniciosumAiktmash;
        }

        HvjTmash = [];
        HljTmash = [];
        for (j = 0; j < numero_etapas; j++) {
            HvjTmash[j] = Rconst * sumaj1ktmash[j] + Rconst * tmash[j] * (z2[j] - 1 - (1 / Bjtmash[j]) * Math.log((z2[j] + Bjtmash[j]) / z2[j]) * (doblesumTotaltmash[j]));
            HljTmash[j] = Rconst * sumaj1ktmash[j] + Rconst * tmash[j] * (z1[j] - 1 - (1 / Bjtmash[j]) * Math.log((z1[j] + Bjtmash[j]) / z1[j]) * (doblesumTotaltmash[j]));
        }
        console.log("HvjTmash");
        console.log(HvjTmash);
        console.log("HljTmash");
        console.log(HljTmash);

        /*ENTALPIA GENERAL*/
        aj1y = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aj1y[i][j] = ((aj0[i] * (TjInit[j] - T0)) + (aj1[i] * ((Math.pow(TjInit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(TjInit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(TjInit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(TjInit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1y");
        console.log(aj1y);

        sumaj1k = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumaj1y = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumaj1y += aj1y[i][j];
            }
            sumaj1k[j] = iniciosumaj1y;
        }
        console.log("sumaj1k");
        console.log(sumaj1k);

        var aij = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                aij[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((TjInit[j] / Tc[i]), 0.5)), 2);
            }
        }
        console.log("aij");
        console.log(aij);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aij y Bij  en función de */
        var Aij = createMatrix();
        var Bij = createMatrix();

        /* START Realiza ecuación Aij y Bij*/

        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                Aij[i][j] = (0.42747 * aij[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(TjInit[j] / Tc[i], 2)));
                Bij[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (TjInit[j] / Tc[i]));
            }
        }
        console.log("Aij");
        console.log(Aij);
        console.log("Bij");
        console.log(Bij);
        /* END Realiza ecuación Aj y Bj */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAj = createMatrix();
        matrizMultiplicacionBj = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (j = 0; j < numero_etapas; j++) {
                matrizMultiplicacionAj[i][j] = Aij[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBj[i][j] = Bij[i][j] * yNormalizado[i][j];
            }
        }

        Aj = [];
        Bj = [];
        for (j = 0; j < numero_etapas; j++) {
            auxAj = 0;
            auxBj = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                auxAj += matrizMultiplicacionAj[i][j];
                auxBj += matrizMultiplicacionBj[i][j];
            }
            Aj.push(auxAj);
            Bj.push(auxBj);
        }

        doblesumAik = createMatrix();
        for (i = 0; i < composicion_entrada.length; i++) {
            for (k = 0; k < composicion_entrada.length; k++) {
                for (j = 0; j < numero_etapas; j++) {
                    doblesumAik[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aij[i][j] * Aij[k][j]) * (1 - mi[i] * Math.pow(TjInit[j] / Tc[i], 0.5) / (2 * aij[i][j]) - mi[k] * Math.pow(TjInit[j] / Tc[k], 0.5) / (2 * aij[k][j]));
                }
            }
        }
        console.log("doblesumAik");
        console.log(doblesumAik);

        doblesumTotal = [];
        for (j = 0; j < numero_etapas; j++) {
            iniciosumAik = 0;
            for (i = 0; i < composicion_entrada.length; i++) {
                iniciosumAik += doblesumAik[i][j];
            }
            doblesumTotal[j] = iniciosumAik;
        }

        Hvj = [];
        Hlj = [];
        for (j = 0; j < numero_etapas; j++) {
            Hvj[j] = Rconst * sumaj1k[j] + Rconst * TjInit[j] * (z2[j] - 1 - (1 / Bj[j]) * Math.log((z2[j] + Bj[j]) / z2[j]) * (doblesumTotal[j]));
            Hlj[j] = Rconst * sumaj1k[j] + Rconst * TjInit[j] * (z1[j] - 1 - (1 / Bj[j]) * Math.log((z1[j] + Bj[j]) / z1[j]) * (doblesumTotal[j]));
        }
        console.log("Hvj");
        console.log(Hvj);
        console.log("Hlj");
        console.log(Hlj);

        /*Derivadas*/
        const F = 1;

        ecuHj = [];
        derivtj0 = [];
        derivtj1L = [];
        derivtj1V = [];
        derivtj2 = [];
        for (j = 0; j < numero_etapas; j++) {
            if (j == (numero_etapas - 1)) {
                ecuHj[j] = Lj[j - 1] * Hlj[j - 1] + F * sumaj1k[j] * Rconst - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] /*- Qj[j]*/;
                derivtj0[j] = (HljTmash[j - 1] - HljTmenosh[j - 1]) / h[j - 1];
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
            }
            else if (j == 0) {
                ecuHj[j] = Vj[j + 1] * Hvj[j + 1] + F * sumaj1k[j] * Rconst - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] /*- Qj[j]*/;
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
                derivtj2[j] = (HvjTmash[j + 1] - HvjTmenosh[j + 1]) / h[j + 1];
            }
            else {
                ecuHj[j] = Lj[j - 1] * Hlj[j - 1] + Vj[j + 1] * Hvj[j + 1] + F * sumaj1k[j] * Rconst - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] /*- Qj[j]*/;
                derivtj0[j] = (HljTmash[j - 1] - HljTmenosh[j - 1]) / h[j - 1];
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
                derivtj2[j] = (HvjTmash[j + 1] - HvjTmenosh[j + 1]) / h[j + 1];
            }
        }
        console.log("ecuHj");
        console.log(ecuHj);
        console.log("derivtj0");
        console.log(derivtj0);
        console.log("derivtj1L");
        console.log(derivtj1L);
        console.log("derivtj1V");
        console.log(derivtj1V);
        console.log("derivtj2");
        console.log(derivtj2);

        /* Inicio Algoritmo de Thomas para DeltaT */
        Ajener = [];
        Bjener = [];
        Cjener = [];
        Djener = [];
        for (j = 0; j < numero_etapas; j++) {
            if (j == (numero_etapas - 1)) { //Ultima etapa
                Ajener[j] = Lj[j - 1] * derivtj0[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Cjener[j] = 0;
                Djener[j] = -ecuHj[j];
            }
            else if (j == 0) { //Primera etapa
                Cjener[j] = Vj[j + 1] * derivtj2[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Djener[j] = -ecuHj[j];
            }
            else { //Intermedias
                Ajener[j] = Lj[j - 1] * derivtj0[j];
                Cjener[j] = Vj[j + 1] * derivtj2[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Djener[j] = -ecuHj[j];
            }
        }
        console.log("Ajener");
        console.log(Ajener);
        console.log("Bjener");
        console.log(Bjener);
        console.log("Cjener");
        console.log(Cjener);
        console.log("Djener");
        console.log(Djener);

        qjiener = [];
        pjiener = [];
        for (j = 0; j < numero_etapas; j++) {

            if (j == 0) { // Primera etapa
                pjiener[j] = Cjener[j] / Bjener[j];
                qjiener[j] = Djener[j] / Bjener[j];
            }
            else if (j == (numero_etapas - 1)) { //Ultima Etapa
                pjiener[j] = 0;
                qjiener[j] = (Djener[j] - Ajener[j] * pjiener[j - 1]) / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
            }
            else { //Intermedio
                pjiener[j] = Cjener[j] / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
                qjiener[j] = (Djener[j] - Ajener[j] * pjiener[j - 1]) / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
            }
        }
        console.log("qjiener");
        console.log(qjiener);
        console.log("pjiener");
        console.log(pjiener);

        deltaTj = [];
        for (j = (numero_etapas - 1); j > -1; j--) {
            if (j == (numero_etapas - 1)) { //Ultima Etapa
                deltaTj[j] = qjiener[j];
            }
            else { //Resto de etapas
                deltaTj[j] = qjiener[j] - pjiener[j] * deltaTj[j + 1];
            }
        }
        console.log("deltaTj");
        console.log(deltaTj);

        /* END Algoritmo de Thomas para DeltaTj */

        /* Inicia calculo T(k+1) */
        Tk = [];
        for (j = 0; j < numero_etapas; j++) {
            Tk[j] = TjInit[j] + deltaTj[j];
        }
        console.log("Tk");
        console.log(Tk);
        /* Termina calculo T(k+1) */

        /* ******************************************************************************************************************** */
        /* ******************************************************************************************************************** */
        /* ******************************************************************************************************************** */

        var Tkit = [];
        for (j = 0; j < numero_etapas; j++) {
            Tkit[j] = Tk[j];
        }
        console.log("Tkit");
        console.log(Tkit);

        var ykit = createMatrix();
        for (j = 0; j < numero_etapas; j++) {
            for (i = 0; i < composicion_entrada.length; i++) {
                ykit[i][j] = yNormalizado[i][j];
            }
        }
        console.log("ykit");
        console.log(ykit);

        var Vjkit = [];
        for (j = 0; j < numero_etapas; j++) {
            Vjkit[j] = Vj[j];
        }
        console.log("Vjkit");
        console.log(Vjkit);

        /*Start iteration*/
        do {
            var aijit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aijit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((Tkit[j] / Tc[i]), 0.5)), 2);
                }
            }

            var Aijit = createMatrix();
            var Bijit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    Aijit[i][j] = (0.42747 * aijit[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(Tkit[j] / Tc[i], 2)));
                    Bijit[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (Tkit[j] / Tc[i]));
                }
            }
            console.log("Aijit");
            console.log(Aijit);
            console.log("Bijit");
            console.log(Bijit);

            /* cálculo de Reglas de Mezcla */
            var matrizMultiplicacionAyit = createMatrix();
            var matrizMultiplicacionByit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    matrizMultiplicacionAyit[i][j] = Aijit[i][j] * ykit[i][j];
                    matrizMultiplicacionByit[i][j] = Bijit[i][j] * ykit[i][j];
                }
            }
            var Ajit = [];
            var Bjit = [];
            for (j = 0; j < numero_etapas; j++) {
                var auxAyit = 0;
                var auxByit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    auxAyit += matrizMultiplicacionAyit[i][j];
                    auxByit += matrizMultiplicacionByit[i][j];
                }
                Ajit[j] = auxAyit;
                Bjit[j] = auxByit;
            }
            console.log("Ajit");
            console.log(Ajit);
            console.log("Bjit");
            console.log(Bjit);

            /* START Ecuación de tercer grado */
            var ait = [];
            var bit = [];
            var cit = [];
            var mpit = [];
            var mqit = [];
            var Deltait = [];
            var z1it = [];
            var z2it = [];
            var z3it = [];
            var mqitpositive = [];
            for (j = 0; j < numero_etapas; j++) {
                ait[j] = -1;
                bit[j] = Ajit[j] - Bjit[j] - Math.pow(Bjit[j], 2);
                cit[j] = -(Ajit[j] * Bjit[j]);
            }
            for (j = 0; j < numero_etapas; j++) {
                mpit[j] = (3 * bit[j] - Math.pow(ait[j], 2)) / 3;
                mqit[j] = (2 * Math.pow(ait[j], 3) - 9 * ait[j] * bit[j] + 27 * cit[j]) / 27;
                Deltait[j] = Math.pow(mqit[j] / 2, 2) + Math.pow(mpit[j] / 3, 3);
            }

            for (j = 0; j < numero_etapas; j++) {
                if (mqit[j] < 0)
                    mqit[j] = mqit[1];
            }

            for (j = 0; j < numero_etapas; j++) {
                z1it[j] = Math.pow(((-mqit[j] / 2) + Math.pow(Deltait[j], 1 / 2)), 1 / 3) + (-1 * Math.pow(((mqit[j] / 2) + Math.pow(Deltait[j], 1 / 2)), 1 / 3)) - (ait[j] / 3);
            }
            for (j = 0; j < numero_etapas; j++) {
                z2it[j] = - (z1it[j] / 2) + Math.sqrt(Math.pow(z1it[j] / 2, 2) + mqit[j] / z1it[j]) - ait[j] / 3;
                z3it[j] = - (z1it[j] / 2) - Math.sqrt(Math.pow(z1it[j] / 2, 2) + mqit[j] / z1it[j]) - ait[j] / 3;
            }
            console.log("bit");
            console.log(bit);
            console.log("mqit");
            console.log(mqit);
            console.log("z1it");
            console.log(z1it);
            console.log("z2it");
            console.log(z2it);
            console.log("z3it");
            console.log(z3it);

            var phiLjit = createMatrix();
            var phiVjit = createMatrix();
            var kijit = createMatrix();
            /* START cálculo de phi */
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    phiVjit[i][j] = Math.exp((z2it[j] - 1) * (Bijit[i][j] / Bjit[j]) - Math.log(Math.abs(z2it[j] - Bjit[j])) - (Ajit[j] / Bjit[j]) * ((2 * Math.pow(Aijit[i][j], 0.5) / Math.pow(Ajit[j], 0.5)) - (Bijit[i][j]) / Bjit[j])) * (Math.log((z2it[j] + Bjit[j]) / z2it[j]));
                    phiLjit[i][j] = Math.exp((z1it[j] - 1) * (Bijit[i][j] / Bjit[j]) - Math.log(Math.abs(z1it[j] - Bjit[j])) - (Ajit[j] / Bjit[j]) * ((2 * Math.pow(Aijit[i][j], 0.5) / Math.pow(Ajit[j], 0.5)) - (Bijit[i][j]) / Bjit[j])) * (Math.log((z1it[j] + Bjit[j]) / z1it[j]));
                }
            }
            console.log("phiVjit");
            console.log(phiVjit);
            console.log("phiLjit");
            console.log(phiLjit);

            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    if (phiVjit[i][j] == 0)
                        phiVjit[i][j] = 1;
                }
            }

            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    if (phiLjit[i][j] == 0)
                        phiLjit[i][j] = 1;
                }
            }

            /* START cálculo Constante de equilibrio */
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    kijit[i][j] = phiLjit[i][j] / phiVjit[i][j];
                }
            }
            console.log("kijit");
            console.log(kijit);
            /* END Constante de equilibrio */

            /* Inicio Algoritmo de Thomas para composiciones liquidas */
            var Ait = createMatrix();
            var Bit = createMatrix();
            var Cit = createMatrix();
            var Dit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    if (j == (numero_etapas - 1)) { //Ultima etapa

                        Ait[i][j] = Vjkit[j] + flujo_gas - Vjkit[0];
                        Bit[i][j] = flujo_gas - Vjkit[0] + (Vjkit[j] * kijit[i][j]);
                        Cit[i][j] = 0;
                        Dit[i][j] = -(flujo_gas * composicion_entrada[i][1]);
                    }
                    else { // Primera etapa e intermedias
                        Ait[i][j] = Vjkit[j] + flujo_solvente - Vjkit[0];
                        Dit[i][j] = -(flujo_solvente * composicion_entrada[i][1]);
                        Cit[i][j] = Vjkit[j + 1] * kijit[i][j + 1];
                        Bit[i][j] = -(Vjkit[j + 1] + flujo_solvente - Vjkit[0] + (Vjkit[j] * kijit[i][j]));
                    }
                }
            }

            var qjiit = createMatrix();
            var pjiit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    if (j == 0) { // Primera etapa
                        pjiit[i][j] = Cit[i][j] / Bit[i][j];
                        qjiit[i][j] = Dit[i][j] / Bit[i][j];
                    }
                    else if (j == (numero_etapas - 1)) { //Ultima Etapa
                        pjiit[i][j] = 0;
                        qjiit[i][j] = (Dit[i][j] - Ait[i][j] * pjiit[i][j - 1]) / (Bit[i][j] - Ait[i][j] * pjiit[i][j - 1]);
                    }
                    else { //Intermedio
                        pjiit[i][j] = Cit[i][j] / (Bit[i][j] - Ait[i][j] * pji[i][j - 1]);
                        qjiit[i][j] = (Dit[i][j] - Ait[i][j] * pjiit[i][j - 1]) / (Bit[i][j] - Ait[i][j] * pjiit[i][j - 1]);
                    }
                }
            }

            var xjiit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = (numero_etapas - 1); j > -1; j--) {
                    if (j == (numero_etapas - 1)) { //Ultima Etapa
                        xjiit[i][j] = qjiit[i][j];
                    }
                    else { //Intermedias y primera
                        xjiit[i][j] = qjiit[i][j] - pjiit[i][j] * xjiit[i][j + 1];
                    }
                }
            }
            console.log("xjiit");
            console.log(xjiit);
            /* END Algoritmo de Thomas para composiciones liquidas */


            /* START Calcula Lj0it */
            var Lj0it = [];
            for (j = 0; j < numero_etapas; j++) {
                if (j == (numero_etapas - 1)) {
                    Lj0it[j] = (flujo_solvente + flujo_gas) - Vjkit[0];
                } else {
                    Lj0it[j] = Vjkit[j + 1] + flujo_solvente - Vjkit[0];
                }
            }
            /* END Calcula Lj0it */

            /* START calculo suma x */
            var sumaXit = [];
            for (j = 0; j < numero_etapas; j++) {
                sumaXinitit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    sumaXinitit += xjiit[i][j];
                }
                sumaXit.push(sumaXinitit);
            }

            var Ljit = [];
            for (j = 0; j < numero_etapas; j++) {
                Ljit[j] = Lj0it[j] * Math.abs(sumaXit[j]);
            }
            /* END calculo Ljit */

            /* START Calcula Vjit */
            var Vjit = [];
            for (j = 0; j < numero_etapas; j++) {
                if (j == 0) {
                    Vjit[j] = -Ljit[numero_etapas - 1] + flujo_solvente + flujo_gas;
                }
                else {
                    Vjit[j] = Ljit[j - 1] - Ljit[numero_etapas - 1] + flujo_gas;
                }
            }
            /* END Calcula Vj */

            /*START Normalización de x & y*/
            var xNormalizadoit = createMatrix();
            var yNormalizadoit = createMatrix();
            var sumaYnormit = [];
            var sumaXnormit = [];
            var yNormit = createMatrix();
            for (j = 0; j < numero_etapas; j++) {
                sumaXAuxit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    sumaXAuxit += xjiit[i][j];
                }
                sumaXnormit.push(sumaXAuxit);
            }

            for (j = 0; j < numero_etapas; j++) {
                for (i = 0; i < composicion_entrada.length; i++) {
                    xNormalizadoit[i][j] = (xjiit[i][j] / sumaXnormit[j]);
                }
            }
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    yNormit[i][j] = kijit[i][j] * xNormalizadoit[i][j];
                }
            }

            //Normalización de Y
            for (j = 0; j < numero_etapas; j++) {
                sumaYAuxit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    sumaYAuxit += yNormit[i][j];
                }
                sumaYnormit.push(sumaYAuxit);
            }
            for (j = 0; j < numero_etapas; j++) {
                for (i = 0; i < composicion_entrada.length; i++) {

                    yNormalizadoit[i][j] = (yNormit[i][j] / sumaYnormit[j]);
                }
            }
            /* END Normalización de x & y */

            tmashit = [];
            tmenoshit = [];
            hit = [];
            for (j = 0; j < numero_etapas; j++) {
                tmashit[j] = Tkit[j] + (Tkit[j] * 1e-6);
                tmenoshit[j] = Tkit[j] - (Tkit[j] * 1e-6);
                hit[j] = 2 * Tkit[j] * 1e-6;
            }

            /*ENTALPIA FUNCION tmenosh[j]*/
            const T0 = 0;
            const Rconst = 1.987;
            aj1ytmenoshit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aj1ytmenoshit[i][j] = ((aj0[i] * (tmenoshit[j] - T0)) + (aj1[i] * ((Math.pow(tmenoshit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmenoshit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmenoshit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmenoshit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1ktmenoshit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumaj1ytmenoshit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumaj1ytmenoshit += aj1ytmenoshit[i][j];
                }
                sumaj1ktmenoshit[j] = iniciosumaj1ytmenoshit;
            }

            var aijtmenoshit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aijtmenoshit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmenoshit[j] / Tc[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmenosh y Bijtmenosh  en función de tmenosH*/
            var Aijtmenoshit = createMatrix();
            var Bijtmenoshit = createMatrix();

            /* START Realiza ecuación Aijtmenosh y Bijtmenosh*/
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    Aijtmenoshit[i][j] = (0.42747 * aijtmenoshit[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(tmenoshit[j] / Tc[i], 2)));
                    Bijtmenoshit[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (tmenoshit[j] / Tc[i]));
                }
            }
            /* END Realiza ecuación Ajtmenosh y Bjtmenosh */

            /* cálculo de Reglas de Mezcla */

            matrizMultiplicacionAjtmenoshit = createMatrix();
            matrizMultiplicacionBjtmenoshit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    matrizMultiplicacionAjtmenoshit[i][j] = Aijtmenoshit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjtmenoshit[i][j] = Bijtmenoshit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajtmenoshit = [];
            Bjtmenoshit = [];
            for (j = 0; j < numero_etapas; j++) {
                auxAjtmenoshit = 0;
                auxBjtmenoshit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    auxAjtmenoshit += matrizMultiplicacionAjtmenoshit[i][j];
                    auxBjtmenoshit += matrizMultiplicacionBjtmenoshit[i][j];
                }
                Ajtmenoshit.push(auxAjtmenoshit);
                Bjtmenoshit.push(auxBjtmenoshit);
            }

            doblesumAiktmenoshit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (k = 0; k < composicion_entrada.length; k++) {
                    for (j = 0; j < numero_etapas; j++) {
                        doblesumAiktmenoshit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijtmenoshit[i][j] * Aijtmenoshit[k][j]) * (1 - mi[i] * Math.pow(tmenoshit[j] / Tc[i], 0.5) / (2 * aijtmenoshit[i][j]) - mi[k] * Math.pow(tmenoshit[j] / Tc[k], 0.5) / (2 * aijtmenoshit[k][j]));
                    }
                }
            }

            doblesumTotaltmenoshit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumAiktmenoshit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumAiktmenoshit += doblesumAiktmenoshit[i][j];
                }
                doblesumTotaltmenoshit[j] = iniciosumAiktmenoshit;
            }

            HvjTmenoshit = [];
            HljTmenoshit = [];
            for (j = 0; j < numero_etapas; j++) {
                HvjTmenoshit[j] = Rconst * sumaj1ktmenoshit[j] + Rconst * tmenoshit[j] * (z2it[j] - 1 - (1 / Bjtmenoshit[j]) * Math.log((z2it[j] + Bjtmenoshit[j]) / z2it[j]) * (doblesumTotaltmenoshit[j]));
                HljTmenoshit[j] = Rconst * sumaj1ktmenoshit[j] + Rconst * tmenoshit[j] * (z1it[j] - 1 - (1 / Bjtmenoshit[j]) * Math.log((z1it[j] + Bjtmenoshit[j]) / z1it[j]) * (doblesumTotaltmenoshit[j]));
            }

            /*ENTALPIA FUNCION tmash[j]*/
            aj1ytmashit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aj1ytmashit[i][j] = ((aj0[i] * (tmashit[j] - T0)) + (aj1[i] * ((Math.pow(tmashit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmashit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmashit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmashit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1ktmashit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumaj1ytmashit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumaj1ytmashit += aj1ytmashit[i][j];
                }
                sumaj1ktmashit[j] = iniciosumaj1ytmashit;
            }

            var aijtmashit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aijtmashit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmashit[j] / Tc[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmash y Bijtmash  en función de tmash*/
            var Aijtmashit = createMatrix();
            var Bijtmashit = createMatrix();

            /* START Realiza ecuación Aijtmash y Bijtmash*/

            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    Aijtmashit[i][j] = (0.42747 * aijtmashit[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(tmashit[j] / Tc[i], 2)));
                    Bijtmashit[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (tmashit[j] / Tc[i]));
                }
            }
            /* END Realiza ecuación Ajtmash y Bjtmash */

            /* Cálculo de Reglas de Mezcla */
            matrizMultiplicacionAjtmashit = createMatrix();
            matrizMultiplicacionBjtmashit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    matrizMultiplicacionAjtmashit[i][j] = Aijtmashit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjtmashit[i][j] = Bijtmashit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajtmashit = [];
            Bjtmashit = [];
            for (j = 0; j < numero_etapas; j++) {
                auxAjtmashit = 0;
                auxBjtmashit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    auxAjtmashit += matrizMultiplicacionAjtmashit[i][j];
                    auxBjtmashit += matrizMultiplicacionBjtmashit[i][j];
                }
                Ajtmashit.push(auxAjtmashit);
                Bjtmashit.push(auxBjtmashit);
            }

            doblesumAiktmashit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (k = 0; k < composicion_entrada.length; k++) {
                    for (j = 0; j < numero_etapas; j++) {
                        doblesumAiktmashit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijtmashit[i][j] * Aijtmashit[k][j]) * (1 - mi[i] * Math.pow(tmashit[j] / Tc[i], 0.5) / (2 * aijtmashit[i][j]) - mi[k] * Math.pow(tmashit[j] / Tc[k], 0.5) / (2 * aijtmashit[k][j]));
                    }
                }
            }

            doblesumTotaltmashit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumAiktmashit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumAiktmashit += doblesumAiktmashit[i][j];
                }
                doblesumTotaltmashit[j] = iniciosumAiktmashit;
            }

            HvjTmashit = [];
            HljTmashit = [];
            for (j = 0; j < numero_etapas; j++) {
                HvjTmashit[j] = Rconst * sumaj1ktmashit[j] + Rconst * tmashit[j] * (z2it[j] - 1 - (1 / Bjtmashit[j]) * Math.log((z2it[j] + Bjtmashit[j]) / z2it[j]) * (doblesumTotaltmashit[j]));
                HljTmashit[j] = Rconst * sumaj1ktmashit[j] + Rconst * tmashit[j] * (z1it[j] - 1 - (1 / Bjtmashit[j]) * Math.log((z1it[j] + Bjtmashit[j]) / z1it[j]) * (doblesumTotaltmashit[j]));
            }

            /*ENTALPIA GENERAL*/
            aj1yit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aj1yit[i][j] = ((aj0[i] * (Tkit[j] - T0)) + (aj1[i] * ((Math.pow(Tkit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(Tkit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(Tkit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(Tkit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1kit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumaj1yit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumaj1yit += aj1yit[i][j];
                }
                sumaj1kit[j] = iniciosumaj1yit;
            }

            var aijit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    aijit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((Tkit[j] / Tc[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aij y Bij  en función de */
            var Aijit = createMatrix();
            var Bijit = createMatrix();

            /* START Realiza ecuación Aij y Bij*/

            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    Aijit[i][j] = (0.42747 * aijit[i][j]) * ((PjInit[j] / Pc[i]) / (Math.pow(Tkit[j] / Tc[i], 2)));
                    Bijit[i][j] = 0.08664 * ((PjInit[j] / Pc[i]) / (Tkit[j] / Tc[i]));
                }
            }
            /* END Realiza ecuación Aj y Bj */

            /* cálculo de Reglas de Mezcla */

            matrizMultiplicacionAjit = createMatrix();
            matrizMultiplicacionBjit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (j = 0; j < numero_etapas; j++) {
                    matrizMultiplicacionAjit[i][j] = Aijit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjit[i][j] = Bijit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajit = [];
            Bjit = [];
            for (j = 0; j < numero_etapas; j++) {
                auxAjit = 0;
                auxBjit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    auxAjit += matrizMultiplicacionAjit[i][j];
                    auxBjit += matrizMultiplicacionBjit[i][j];
                }
                Ajit.push(auxAjit);
                Bjit.push(auxBjit);
            }

            doblesumAikit = createMatrix();
            for (i = 0; i < composicion_entrada.length; i++) {
                for (k = 0; k < composicion_entrada.length; k++) {
                    for (j = 0; j < numero_etapas; j++) {
                        doblesumAikit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijit[i][j] * Aijit[k][j]) * (1 - mi[i] * Math.pow(Tkit[j] / Tc[i], 0.5) / (2 * aijit[i][j]) - mi[k] * Math.pow(Tkit[j] / Tc[k], 0.5) / (2 * aijit[k][j]));
                    }
                }
            }

            doblesumTotalit = [];
            for (j = 0; j < numero_etapas; j++) {
                iniciosumAikit = 0;
                for (i = 0; i < composicion_entrada.length; i++) {
                    iniciosumAikit += doblesumAikit[i][j];
                }
                doblesumTotalit[j] = iniciosumAikit;
            }

            Hvjit = [];
            Hljit = [];
            for (j = 0; j < numero_etapas; j++) {
                Hvjit[j] = Rconst * sumaj1kit[j] + Rconst * Tkit[j] * (z2it[j] - 1 - (1 / Bjit[j]) * Math.log((z2it[j] + Bjit[j]) / z2it[j]) * (doblesumTotalit[j]));
                Hljit[j] = Rconst * sumaj1kit[j] + Rconst * Tkit[j] * (z1it[j] - 1 - (1 / Bjit[j]) * Math.log((z1it[j] + Bjit[j]) / z1it[j]) * (doblesumTotalit[j]));
            }


            /*Derivadas*/
            const F = 1;

            ecuHjit = [];
            derivtj0it = [];
            derivtj1Lit = [];
            derivtj1Vit = [];
            derivtj2it = [];
            for (j = 0; j < numero_etapas; j++) {
                if (j == (numero_etapas - 1)) {
                    ecuHjit[j] = Ljit[j - 1] * Hljit[j - 1] + F * sumaj1kit[j] * Rconst - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] /*- Qj[j]*/;
                    derivtj0it[j] = (HljTmashit[j - 1] - HljTmenoshit[j - 1]) / hit[j - 1];
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                }
                else if (j == 0) {
                    ecuHjit[j] = Vjit[j + 1] * Hvjit[j + 1] + F * sumaj1kit[j] * Rconst - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] /*- Qj[j]*/;
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                    derivtj2it[j] = (HvjTmashit[j + 1] - HvjTmenoshit[j + 1]) / hit[j + 1];
                }
                else {
                    ecuHjit[j] = Ljit[j - 1] * Hljit[j - 1] + Vjit[j + 1] * Hvjit[j + 1] + F * sumaj1kit[j] * Rconst - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] /*- Qj[j]*/;
                    derivtj0it[j] = (HljTmashit[j - 1] - HljTmenoshit[j - 1]) / hit[j - 1];
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                    derivtj2it[j] = (HvjTmashit[j + 1] - HvjTmenoshit[j + 1]) / hit[j + 1];
                }
            }

            /* Inicio Algoritmo de Thomas para DeltaT */
            Ajenerit = [];
            Bjenerit = [];
            Cjenerit = [];
            Djenerit = [];
            for (j = 0; j < numero_etapas; j++) {
                if (j == (numero_etapas - 1)) { //Ultima etapa
                    Ajenerit[j] = Ljit[j - 1] * derivtj0it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Cjenerit[j] = 0;
                    Djenerit[j] = -ecuHjit[j];
                }
                else if (j == 0) { //Primera etapa
                    Cjenerit[j] = Vjit[j + 1] * derivtj2it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Djenerit[j] = -ecuHjit[j];
                }
                else { //Intermedias
                    Ajenerit[j] = Ljit[j - 1] * derivtj0it[j];
                    Cjenerit[j] = Vjit[j + 1] * derivtj2it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Djenerit[j] = -ecuHjit[j];
                }
            }

            qjienerit = [];
            pjienerit = [];
            for (j = 0; j < numero_etapas; j++) {

                if (j == 0) { // Primera etapa
                    pjienerit[j] = Cjenerit[j] / Bjenerit[j];
                    qjienerit[j] = Djenerit[j] / Bjenerit[j];
                }
                else if (j == (numero_etapas - 1)) { //Ultima Etapa
                    pjienerit[j] = 0;
                    qjienerit[j] = (Djenerit[j] - Ajenerit[j] * pjienerit[j - 1]) / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                }
                else { //Intermedio
                    pjienerit[j] = Cjenerit[j] / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                    qjienerit[j] = (Djenerit[j] - Ajenerit[j] * pjienerit[j - 1]) / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                }
            }

            deltaTjit = [];
            for (j = (numero_etapas - 1); j > -1; j--) {
                if (j == (numero_etapas - 1)) { //Ultima Etapa
                    deltaTjit[j] = qjienerit[j];
                }
                else { //Resto de etapas
                    deltaTjit[j] = qjienerit[j] - pjienerit[j] * deltaTjit[j + 1];
                }
            }
            /* END Algoritmo de Thomas para DeltaTj */

            /* Inicia calculo T(k+1) y convergencia */
            for (j = 0; j < numero_etapas; j++) {
                Tkit[j] = Tkit[j] + deltaTjit[j];
            }

            taoj = [];
            for (j = 0; j < numero_etapas; j++) {
                taoj[j] = Math.pow(deltaTjit[j], 2);
            }

            var conv_criteria;
            initsumConv = 0;
            for (j = 0; j < numero_etapas; j++) {
                initsumConv += taoj[j];
            }
            conv_criteria = initsumConv;
            /* Termina calculo T(k+1) y convergencia */

        }
        while (conv_criteria <= (0.01 * numero_etapas));

        console.log("conv_criteria");
        console.log(conv_criteria);

        /* ********************************************************************************************************************* */
        /* ********************************************************************************************************************* */
        /* ********************************************************************************************************************* */

        function createMatrix() {
            var matriz = new Array(composicion_entrada.length);
            for (var i = 0; i < matriz.length; i++) {
                matriz[i] = new Array(parseInt(numero_etapas));
            }
            return matriz;
        }



    } // END Validation






    var end_time = Date.now();
    console.log("Tiempo de ejecución: " + (end_time - start_time) + " ms.")
}

$(document).ready(function () {
    $("#metano_checkbox").change(function () {
        if (this.checked) {
            $("#metano_input").prop('disabled', false);
        } else {
            $("#metano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#etano_checkbox").change(function () {
        if (this.checked) {
            $("#etano_input").prop('disabled', false);
        } else {
            $("#etano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#propano_checkbox").change(function () {
        if (this.checked) {
            $("#propano_input").prop('disabled', false);
        } else {
            $("#propano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#butano_checkbox").change(function () {
        if (this.checked) {
            $("#butano_input").prop('disabled', false);
        } else {
            $("#butano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#pentano_checkbox").change(function () {
        if (this.checked) {
            $("#pentano_input").prop('disabled', false);
        } else {
            $("#pentano_input").prop('disabled', true).val("0.0");
        }
    });

    $("#sulfuro_de_hidrogeno_checkbox").change(function () {
        if (this.checked) {
            $("#sulfuro_de_hidrogeno_input").prop('disabled', false);
        } else {
            $("#sulfuro_de_hidrogeno_input").prop('disabled', true).val("0.0");
        }
    });

    $("#dioxido_de_carbono_checkbox").change(function () {
        if (this.checked) {
            $("#dioxido_de_carbono_input").prop('disabled', false);
        } else {
            $("#dioxido_de_carbono_input").prop('disabled', true).val("0.0");
        }
    });
    $("#aire_checkbox").change(function () {
        if (this.checked) {
            $("#aire_input").prop('disabled', false);
        } else {
            $("#aire_input").prop('disabled', true).val("0.0");
        }
    });

    $("#limpiar_campos").click(function () {
        $("#flujo_solvente").val('');
        $("#temperatura_solvente").val('');
        $("#presion_solvente").val('');
        $("#flujo_gas").val('');
        $("#temperatura_gas").val('');
        $("#presion_gas").val('');
        $("#metano_input").val('').prop('disabled', true);
        $("#etano_input").val('').prop('disabled', true);
        $("#propano_input").val('').prop('disabled', true);
        $("#butano_input").val('').prop('disabled', true);
        $("#pentano_input").val('').prop('disabled', true);
        $("#sulfuro_de_hidrogeno_input").val('').prop('disabled', true);
        $("#dioxido_de_carbono_input").val('').prop('disabled', true);
        $("#aire_input").val('').prop('disabled', true);



        $("#presion_domo").val('');
        $("#presion_fondo").val('');
        $("#numero_etapas").val('');
        $('input:checkbox').removeAttr('checked');
        // $("#resultados").css("display", "none");
    });

    $("#numero_etapas").change(function () {
        var etapas = $("#numero_etapas").val();
        etapas = parseInt(etapas);
        aux = ""
        for (var i = 1; i < (etapas + 1); i++) {
            aux += `                                                            <tr>
            <td>`+ i + `</td>
            <td>
                <div class="form-group" style="margin-bottom: 0px; margin-top: 1px;">
                    <input type="number" class="form-control"
                        value="0.0" id="etapa_`+ i + `">
                </div>
            </td>
        </tr>`;
        }
        $("#tablaEtapas").html(aux);
    });



});


function imprimirResultados(componenetesEntrada, etapas, xNormalizados, kij, tj, yNormalizados, vj2, lj2) {
    var tituloComposicion = '<tr><th scope="col">Componentes</th>';
    var tituloVaporComposicion = '<tr><th scope="col">Componentes</th>';
    var tituloKij = '<tr><th scope="col">Componenetes</th>';
    for (i = 0; i < etapas; i++) {
        tituloComposicion += '<th scope="col">' + (i + 1) + '</th>';
        tituloVaporComposicion += '<th scope="col">' + (i + 1) + '</th>';
        tituloKij += '<th scope="col">' + (i + 1) + '</th>';
    }
    tituloComposicion += '</tr>';
    tituloVaporComposicion += '</tr>';
    tituloKij += '</tr>';
    $("#resultadosComposicionTitulo").html(tituloComposicion);
    $("#resultadosComposicionVaporTitulo").html(tituloVaporComposicion);
    $("#resultadosKijTitulo").html(tituloKij);

    bodyComposicion = '';
    bodyVaporComposicion = '';
    auxComposicion = '';
    auxVaporComposicion = '';
    bodyKij = '';
    auxKij = '';
    for (i = 0; i < componenetesEntrada.length; i++) {
        bodyVaporComposicion = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        bodyComposicion = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        bodyKij = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        for (j = 0; j < etapas; j++) {
            bodyComposicion += '<td>' + xNormalizados[i][j] + '</td>';
            bodyVaporComposicion += '<td>' + yNormalizados[i][j] + '</td>';
            bodyKij += '<td>' + kij[i][j] + '</td>';
        }
        bodyComposicion += '</tr>';
        bodyVaporComposicion += '</tr>';
        auxComposicion += bodyComposicion;
        auxVaporComposicion += bodyVaporComposicion;
        bodyKij += '</tr>';
        auxKij += bodyKij;
    }
    $("#resultadosComposicionCuerpo").html(auxComposicion);
    $("#resultadosComposicionVaporCuerpo").html(auxVaporComposicion);
    $("#resultadosKijCuerpo").html(auxKij);

    titulo = '<tr>';
    cuerpo = '<tr>';
    titulovj = '<tr>';
    cuerpovj = '<tr>';
    titulolj = '<tr>';
    cuerpolj = '<tr>';
    for (j = 0; j < etapas; j++) {
        titulo += '<th>' + (j + 1) + '</th>';
        cuerpo += '<td>' + tj[j] + '</td>';

        titulovj += '<th>' + (j + 1) + '</th>';
        cuerpovj += '<td>' + vj2[j] + '</td>';

        titulolj += '<th>' + (j + 1) + '</th>';
        cuerpolj += '<td>' + lj2[j] + '</td>';
    }
    titulo += '</tr>';
    cuerpo += '</tr>';
    titulovj += '</tr>';
    cuerpovj += '</tr>';
    titulolj += '</tr>';
    cuerpolj += '</tr>';
    $("#resultadosTemperaturaTitulo").html(titulo);
    $("#resultadosTemperaturaCuerpo").html(cuerpo);

    $("#resultadosVjTitulo").html(titulovj);
    $("#resultadosVjCuerpo").html(cuerpovj);

    $("#resultadosLjTitulo").html(titulolj);
    $("#resultadosLjCuerpo").html(cuerpolj);


    $("#resultados").css("display", "flex");

    etapasVector = [];

    for (j = 0; j < etapas; j++) {
        etapasVector.push((j + 1));
    }

    var datosgraficaLiquido = [];
    var datosgraficaVapor = [];
    var datosgraficaKij = [];
    for (i = 0; i < componenetesEntrada.length; i++) {
        datos = [];
        datosVapor = [];
        datosKij = [];
        for (j = 0; j < etapas; j++) {
            datos.push([xNormalizados[i][j]]);
            datosVapor.push([yNormalizados[i][j]]);
            datosKij.push([kij[i][j]]);
        }
        datosgraficaLiquido.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datos
        });

        datosgraficaVapor.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datosVapor
        });
        datosgraficaKij.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datosKij
        });
    }
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
    var xNormalizadoChart = document.getElementById("xNormalizadoChart");
    var myLineChart = new Chart(xNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaLiquido,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var yNormalizadoChart = document.getElementById("yNormalizadoChart");
    var myLineChart = new Chart(yNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaVapor,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });

    var temperaturaChart = document.getElementById("temperaturaChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: tj,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var temperaturaChart = document.getElementById("LjChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: lj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });




    var vjChart = document.getElementById("VjChart");
    var myLineChart5 = new Chart(vjChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: vj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var kChart = document.getElementById("kChart");
    var myLineChart3 = new Chart(kChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaKij,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });
}
